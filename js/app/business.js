tue.business = tue.business || {};

tue.business.marketingCostExponent = 1.5;
tue.business.marketingVisitationScale = 0.5;
tue.business.advertisingCostExponent = 1.5;

tue.business.Business =
	function(empire, definition, name)
	{
		this.empire = empire;
		this.def = definition;
		this.name = name;
		this.cash = Math.max(this.def.cost*Math.pow(3, this.empireUpgrade("startingCash").owned) - 1, 0);
		this.products = [];
		this.customerAccum = new tue.Accumulator(1, 0,
			function(customers)
			{
				this.products.forEach(
					function(product)
					{
						product.update(customers);
					});
			}.bind(this));
		
		tue.defineUpgrade(this, "marketing", this.def.marketingCost, tue.business.marketingCostExponent, 10, "Marketing", "Increases the rate at which customers visit your business.");
		tue.defineUpgrade(this, "advertising", this.def.advertisingCost, tue.business.advertisingCostExponent, 10, "Advertising", "Improves the effectiveness of sales upgrades.");

		this.upgrades.marketing.owned = this.empireUpgrade("marketingSynergy").owned;
		this.upgrades.advertising.owned = this.empireUpgrade("advertisingSynergy").owned;

		for(var index = 0;index < this.def.productDefs.length;++index)
		{
			var productDef = this.def.productDefs[index];
			var product = new tue.product.Product(this, productDef);
			product.upgrades.contract.owned = this.empireUpgrade("contractSynergy").owned;
			product.upgrades.sales.owned = this.empireUpgrade("salesSynergy").owned;
			this.products.push(product);
		}
		
		Object.defineProperty(this, "worth",
			{
				get:function()
					{
						return this.def.cost*10;
					}
			});
		Object.defineProperty(this, "customerRate",
			{
				get:function()
					{
						var ups = this.upgrades.marketing.owned;
						return this.def.customerRate*(1 + ups*tue.business.marketingVisitationScale);
					}
			});
		Object.defineProperty(this, "cashRate",
			{
				//get:this.productSum.bind(this, "cashRate")
				get:function()
					{
						return this.cashRateGivenInitial(this.cash);
					}
			});
		Object.defineProperty(this, "idealCashRate",
			{
				//get:this.productSum.bind(this, "cashRate")
				get:function()
					{
						return this.cashRateGivenInitial();
					}
			});
	};

tue.business.Business.prototype.sell =
	function()
	{
		//add appreciated value of business to controller.capital
		//appreciated value should be dependent on customer base and purchased upgrades, and possibly cash on hand (up to a cap)
		this.empire.sell(this);
	};

tue.business.Business.prototype.tick =
	function()
	{
		this.customerAccum.accumulate(this.customerRate);
		this.customerAccum.discharge();
	};

tue.business.Business.prototype.productSum =
	function(attribute)
	{
		var s = 0;
		this.products.forEach(
			function(product)
			{
				s += product[attribute];
			});
		return s;
	};

tue.business.Business.prototype.cashRateGivenInitial =
	function(initial)
	{
		initial = Math.min(initial, this.productSum("costRate"));
		var cash = initial;
		for(var index = 0;index < this.products.length;++index)
		{
			var product = this.products[index];
			var buying = Math.min(cash/product.cost, product.stockRate);
			var selling = Math.min(buying, product.totalSellRate);
			cash +=  selling*product.value - buying*product.cost;
		}
		return cash - initial;
	};

tue.business.Business.prototype.empireUpgrade =
	function(label)
	{
		var result = this.empire.upgrades[this.def.typeName + "." + label];
		if(result === undefined)
		{
			result = { owned: 0 };
		}
		return result;
	};

tue.business.Business.prototype.synchronise =
	function(actual)
	{
		this.cash = actual.cash;
		this.customerAccum.synchronise(actual.customerAccum);

		for(var index = 0;index < this.products.length;++index)
		{
			this.products[index].synchronise(actual.products[index]);
		}
		for(var label in this.upgrades)
		{
			this.upgrades[label].synchronise(actual.upgrades[label]);
		}
	};

tue.business.Business.prototype.encode =
	function()
	{
		var result = {};
		result.def = tue.business.BusinessDefs.indexOf(this.def);
		result.name = this.name;
		result.cash = this.cash;

		result.customerAccum = this.customerAccum.encode();

		result.products = this.products.map(function(product) { return product.encode(); });

		result.upgrades = this.encodeUpgrades();

		return result;
	};

tue.business.Business.decode =
	function(data)
	{
		var result = new tue.business.Business(data.empire, tue.business.BusinessDefs[data.def], data.name);
		result.cash = data.cash;

		tue.Accumulator.decode(data.customerAccum, result.customerAccum);

		result.products = data.products.map(function(data) { data.business = result; return tue.product.Product.decode(data) });

		result.decodeUpgrades(data.upgrades);

		return result;
	};