tue.business = tue.business || {};

tue.business.BusinessDefs =
[
/* Tier 0 - Lemonade Stand */
	{
		typeName: "Lemonade Stand",
		description: "This is the traditional first business in this sort of game. You pride yourself on ruthlessly outcompeting the small children next door. Still, you'll want to move on to better things as soon as possible.",
		isTutorial: true,
		nameList: ["Lemonade Stand", "Lemons 'r' Us", "When Life Gives You Lemons", "Definitely Not a Laundering Operation For the Mafia", "Socially-accepted Child Labour Inc.", "I Don't Give a Citrus", ],
		cost: 0, //that's right, it's free you lucky motherfucker
		customerRate: 2e-1, //same initial rate as the Garage Sale
		marketingCost: 0.5,
		maxWorth: 1e0, //this makes this business extremely unprofitable as a source of capital - intended to be made and sold first and once only, or as a backup in case people run out of capital
		productDefs:
			[
				{
					name: "Lemonade",
					description: "What exactly were you expecting to sell in a lemonade stand?",
					cost: 0.05,
					value: 0.10, //generous markup, because this is the "tutorial" business
					stockRate: 0.1,
					sellRate: 1,
					contractCost: 0.1,
					salesCost: 0.1,
					initialStock: 0
				}
			]
	},

/* Tier 1 - Garage Sale */
	{
		typeName: "Garage Sale",
		description: "This is exactly what you need to earn money to buy cool new things to sell at a future garage sale when they're not cool or new and also a little smellier than you really want out of <portfolio>.",
		nameList: ["Water Damage Sale", "House Repossession Sale", "Indian Burial Ground Moving Sale", "Voodoo Curse Sale", "Neighbours Always Ruin My Gardenias Moving Sale", "Unexpected Medical Bills Sale", "In Debt To The Mob Sale", "Religious Conversion Sale", "Manic Episode Sale", "Deceased Estate Sale", "Not Actually My House Sale", "Expensive Habit Sale", "Bank Lost My Savings Sale", "<Insert Personal Misfortune> Sale", "No Particular Reason Sale"],
		cost: 1e0, //1e0 is 1, but fuck da police
		customerRate: 2e-1, //scales up gradually to max of 10x the specified, but "synergies" and "strategies" can multiply it further
		marketingCost: 0.5,
		maxWorth: 1e1,
		productDefs:
			[
				{
					name: "\"Collectible\" Toys",
					description: "You bought these because they were advertised as collectible. The only thing they've collected is dust.",
					cost: 0.1,
					value: 0.11,
					stockRate: 0.1,
					sellRate: 1,
					contractCost: 0.1,
					salesCost: 0.1
				},
				{
					name: "Unfashionable Clothing",
					description: "These clothes are so horrendously outdated that you can't even donate them as rags.",
					cost: 1,
					value: 1.1,
					stockRate: 0.1,
					sellRate: 0.75,
					contractCost: 1,
					salesCost: 1
				},
				{
					name: "Failed-Hobby Equipment",
					description: "It's truly remarkable how bad you were at so many different things. Still, you should get a good price for this stuff.",
					cost: 10,
					value: 11,
					stockRate: 0.1,
					sellRate: 0.5,
					contractCost: 10,
					salesCost: 10
				},
				{
					name: "Treasured Family Heirlooms",
					description: "Sorry, Grandpa, but war medals sell for a lot these days.",
					cost: 100,
					value: 110,
					stockRate: 0.1,
					sellRate: 0.25,
					contractCost: 100,
					salesCost: 100
				}
			]
	},

/* Tier 2 - Market Stall */
	{
		typeName: "Market Stall",
		description: "It's a little hard to compete for the attention of your shoppers in this marketplace, even with the high-quality <portfolio> you're selling.",
		nameList: ["Sneed's Feed 'n' Seed", "Artisanal Name Cards"],
		cost: 1e2,
		customerRate: 1e0,
		marketingCost: 5e1,
		maxWorth: 1e3,
		productDefs:
			[
			]
	},

/* Tier 3 - E-tailer */
	{
		typeName: "E-tailer",
		description: "Here you are, on the cutting edge of technology. Your biggest problem is getting market share, and keeping track of all your <portfolio>.",
		nameList: ["Sexy Singles In Your Area"],
		cost: 1e4,
		customerRate: 1e1,
		marketingCost: 5e3,
		maxWorth: 1e5,
		productDefs:
			[
			]
	},

/* Tier 4 - Travelling Sales Team */
	{
		typeName: "Travelling Sales Team",
		description: "You're no longer a one-person operation. Whether this is a good thing remains to be seen, but at least you can sell more <portfolio>.",
		nameList: ["Sparkle Motion"],
		cost: 1e7,
		customerRate: 1e2,
		marketingCost: 5e6,
		maxWorth: 1e8,
		productDefs:
			[
			]
	},

/* Tier 5 - Brick and Mortar Retailer */
	{
		typeName: "Brick and Mortar Retailer",
		description: "Your first permanent piece of real estate. Fill it to the brim with <portfolio> and get to selling!",
		nameList: ["Oh-God-Please-Buy-Local 'r' Us"],
		cost: 1e10,
		customerRate: 1e3,
		marketingCost: 5e9,
		maxWorth: 1e11,
		productDefs:
			[
			]
	},

/* Tier 6 - Franchise */
	{
		typeName: "Franchise",
		description: "Now other people will be paying you for the privilege of selling <portfolio>. Living the dream, right here.",
		nameList: ["McBucks Bell Hut"],
		cost: 1e14,
		customerRate: 1e15,
		marketingCost: 5e13,
		maxWorth: 1e10,
		productDefs:
			[
			]
	},

/* Tier 7 - Multinational */
	{
		typeName: "Multinational",
		description: "People all over the world just can't get enough of your <portfolio>.",
		nameList: ["Souls of the Damned International"],
		cost: 1e18,
		customerRate: 1e7,
		marketingCost: 5e17,
		maxWorth: 1e19,
		productDefs:
			[
			]
	},

/* Tier 8 - Omnicorp */
	{
		typeName: "Omnicorp",
		description: "Everyone everywhere buys your products, and you're branching out from <portfolio>. Your company is practically a religion.",
		nameList: ["Globo-Corp"],
		cost: 1e23,
		customerRate: 1e9,
		marketingCost: 5e22,
		maxWorth: 1e24,
		productDefs:
			[
			]
	},

/* Tier 9 - Globucorp */
	{
		typeName: "Globucorp",
		description: "We are the company, and the company is us. We love <portfolio>.",
		nameList: ["Zombo.com"],
		cost: 1e28,
		customerRate: 7.125e9, //estimated world population 2013 - maybe start lower than this, with this as an asymptote
		marketingCost: 5e27,
		maxWorth: 1e29,
		productDefs:
			[
			]
	},
	
/* Tier 10 - Infinicorp */
	{
		//should add a flag to make this hidden until all other upgrades are purchased or something
		typeName: "Infinicorp",
		description: "One planet was never enough. From the beginning, it was all building to this. Your empire is now a Kardashev II civilisation. Congratulations, you're a merchant Galactus.",
		isInfinite: true,
		nameList: [""],
		cost: 1e35,
		customerRate: 1e12, //~1000 earthlike planets
		marketingCost: 5e34,
		maxWorth: 1e36,
		productDefs: 
			[
			]
	}
];