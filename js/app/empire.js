tue.empire = tue.empire || {};

tue.empire.Empire =
	function()
	{
		this.name = "Capitalists Incarcerated";
		this.businesses = [];
		this.cash = 1000; //would be 0 in actual game
		this.portfolio = "paperclips";

		tue.defineUpgrade(this, "Empire.organisation", 100, 4, 9, "Organisation", "Lets you manage more businesses simultaneously.");

		tue.business.BusinessDefs.forEach(
			function(definition, index)
			{
				if(!definition.isTutorial) //tutorial business has no empire upgrades
				{
					if(!definition.isInfinite) //infinite business can't be sold, so "on purchase" upgrades are useless
					{
						tue.defineUpgrade(this, definition.typeName + ".startingCash", definition.cost, 2, 5, "Starting Cash", "Increases how much cash newly purchased " + definition.typeName + "s will have.");
						tue.defineUpgrade(this, definition.typeName + ".marketingSynergy", definition.cost*2, 4, 4, "Marketing Synergy", "Determines the initial level of the marketing upgrade for new " + definition.typeName + "s. Also facilitates a dialogue of holistic empowerment.");
						tue.defineUpgrade(this, definition.typeName + ".advertisingSynergy", definition.cost*2, 4, 4, "Advertising Synergy", "Determines the initial level of the advertising upgrade for new " + definition.typeName + "s. Also architects an intuitive paradigm for proactive incentivisation.");
						tue.defineUpgrade(this, definition.typeName + ".salesSynergy", definition.cost*3, 3, 4, "Salespersonship Synergy", "Determines the initial level of the sales strategy upgrade for all products in new " + definition.typeName + "s. Also revolutionises a frictionless network for optimal ROI.");
						tue.defineUpgrade(this, definition.typeName + ".contractSynergy", definition.cost*3, 3, 4, "Contractual Synergy", "Determines the initial level of the supply contract upgrade for all products in new " + definition.typeName + "s. Also streamlines your fungible catalysts to optimise leverage.");
					}

					//tue.defineUpgrade(this, definition.typeName + ".upgradeManager", definition.cost*
				}
			}, this);

		Object.defineProperty(this, "businessLimit",
			{
				get:function()
					{
						return this.upgrades["Empire.organisation"].owned + 1;
					}
			});
		Object.defineProperty(this, "canPurchaseBusiness",
				{
					get:function()
						{
							return this.businesses.length < this.businessLimit;
						}
				});
	};

tue.empire.Empire.prototype.tick =
	function()
	{
		this.businesses.forEach(function(business){ business.tick(); });
	};

tue.empire.Empire.prototype.buy =
	function(definition, name, silent)
	{
		if(this.canPurchaseBusiness)
		{
			if(!!definition.isInfinite)
			{
				console.log("TODO: disallow purchasing multiple infinite businesses of the same type");
			}
			if(this.cash >= definition.cost)
			{
				var business = new tue.business.Business(this, definition, name);
				this.cash -= definition.cost;
				this.businesses.push(business)
				return business;
			}
			else if(!silent)
			{
				tue.alert.notify(definition.typeName + " costs " + tue.format(definition.cost) + " capital, you only have " + tue.format(this.cash) + ".", "warning", 5000);
				return null;
			}
		}
		else if(!silent)
		{
			tue.alert.notify("Can't purchase more businesses now; you own " + this.businesses.length + " out of " + this.businessLimit + " maximum. Upgrade your Empire's Organisation or sell a business.", "warning", 5000);
			return null;
		}
	};

tue.empire.Empire.prototype.sell =
	function(business)
	{
		if(tue.arrayRemove(business, this.businesses))
		{
			this.cash += business.worth;
		}
		else
		{
			tue.alert.notify("Attempted to sell business " + business.name + " that was not present.", "warning", 5000);
		}
	};

tue.empire.Empire.prototype.encode =
	function()
	{
		var result = {};

		result.name = this.name;
		result.cash = this.cash;
		result.portfolio = this.portfolio;

		result.businesses = this.businesses.map(function(business) { return business.encode(); });

		result.upgrades = this.encodeUpgrades();

		return result;
	};

tue.empire.Empire.decode =
	function(data)
	{
		var result = new tue.empire.Empire();

		result.name = data.name;
		result.cash = data.cash || 0;
		result.portfolio = data.portfolio;

		result.businesses = data.businesses.map(function(data) { data.empire = result; return tue.business.Business.decode(data) });

		result.decodeUpgrades(data.upgrades);

		return result;
	};