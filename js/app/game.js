tue.app
	.controller("GameController", ["$rootScope", "$scope", "$timeout", "$modal", "$log", "AlertService", "DataService",
		function($rootScope, $scope, $timeout, $modal, $log, AlertService, DataService)
		{
			this.format = tue.format;

			this.potentialBusiness = null;
			this.potentialAction = null;
			var _empire = null;
			var _empireView = DataService.register("empire");
			var _activeBusiness = null;
			var _buyAmount = 1;
			var _refundAmount = 1;

			Object.defineProperty(this, "empire",
				{
					get:function()
						{
							return _empire;
						},
					set:function(value)
						{
							_empire = value;
							_empireView.set("empire", _empire);
						}
				});
			Object.defineProperty(this, "activeBusiness",
				{
					get:function()
						{
							return _activeBusiness;
						},
					set:function(value)
						{
							_activeBusiness = value;
							if(_activeBusiness === null)
							{
								this.potentialBusiness = null;
							}
							else
							{
								this.potentialBusiness = new tue.business.Business(this.empire, _activeBusiness.def, _activeBusiness.name);
							}
							this.updatePotentialBusiness();
						}
				});
			Object.defineProperty(this, "buyAmount",
				{
					get:function()
						{
							return _buyAmount;
						},
					set:function(value)
						{
							if(isNaN(value) || !isFinite(value))
							{
								value = 1;
							}
							_buyAmount = Math.min(Math.round(value), 1);
						}
				});
			Object.defineProperty(this, "refundAmount",
				{
					get:function()
						{
							return _refundAmount;
						},
					set:function(value)
						{
							if(isNaN(value) || !isFinite(value))
							{
								value = 1;
							}
							_refundAmount = Math.min(Math.round(value), 1);
						}
				});
			this.checkBusinessCount =
				function(silent)
				{
					if(!this.empire.canPurchaseBusiness)
					{
						if(!silent)
						{
							AlertService.notify("You already own too many businesses to purchase another. Sell one and try again.", "warning", 5000);
						}
						return false;
					}
					return true;
				};
			this.checkBusinessCost =
				function(definition, silent)
				{
					if(definition.cost > this.capital)
					{
						if(!silent)
						{
							AlertService.notify(definition.typeName + " costs " + this.format(definition.cost) + " capital, you only have " + this.format(this.capital) + ".", "warning", 5000);
						}
						return false;
					}
					return true;
				};

			this.describe =
				function(string)
				{
					return string.replace(/<portfolio>/g, this.empire.portfolio);
				};

			this.purchaseBusiness =
				function(definition, name)
				{
					var business = this.empire.buy(definition, name, false);
					if(business)
					{
						this.activeBusiness = business;
					}
					else
					{
						console.log("wat");
						console.log(business);
					}
				};

			this.purchaseBusinessModal =
				function()
				{
					if(!this.checkBusinessCount())
					{
						return false;
					}
					var instance = $modal.open(
						{
							templateUrl: 'templates/purchasebusiness.html',
							controller: 'BusinessPurchaseController as bpurchaser',
							scope:$scope,
							size: 'lg',
							resolve:
								{
									businessDefs:
										function()
										{
											return tue.business.BusinessDefs;
										}
								}
						});

					instance.result.then(
						(function(result)
						{
							if(result.selected !== null)
							{
								this.purchaseBusiness(result.selected, result.name);
							}
						}).bind(this));
				};

			this.updatePotentialBusiness =
				function()
				{
					if(this.activeBusiness !== null)
					{
						this.potentialBusiness.synchronise(this.activeBusiness);
						if(this.potentialAction !== null)
						{
							this.potentialAction();
						}
					}
				};

			this.save =
				function()
				{
					var encoded = angular.toJson(this.empire.encode());
					var compressed = LZString.compressToUTF16(encoded);
					localStorage.setItem("tueEmpireSave", compressed);
					AlertService.notify("Game saved.", "success", 1000);
				};

			this.load =
				function()
				{
					var saved = localStorage.getItem("tueEmpireSave");
					if(saved === null)
					{
						$log.debug("No save detected.");
					}
					var decompressed = LZString.decompressFromUTF16(saved);
					try
					{
						var decoded = tue.empire.Empire.decode(angular.fromJson(decompressed));
						this.empire = decoded;
						if(this.empire.businesses.length > 0)
						{
							this.activeBusiness = this.empire.businesses[0];
						}
						else
						{
							this.activeBusiness = null;
						}
						AlertService.notify("Game loaded.", "success", 1000);
					}
					catch(error)
					{
						this.empire = new tue.empire.Empire();
						$log.debug(error); //most common error will be empty save string
					}
				};

			this.deleteSave =
				function()
				{
					localStorage.setItem("tueEmpireSave", "");
				};

			$scope.$on(tue.events.potentialActionSet,
				(function(event, action)
				{
					this.potentialAction = action;
					this.updatePotentialBusiness();
				}).bind(this));

			$scope.$on(tue.events.potentialActionClear,
				(function(event, action)
				{
					if(this.potentialAction === action)
					{
						this.potentialAction = null;
						this.updatePotentialBusiness();
					}
				}).bind(this));

			this.tick =
				(function()
				{
					this.empire.tick();
					$rootScope.$broadcast("tick");
					$timeout(this.tick, 1000);
				}).bind(this);

			this.animationTick =
				(function(timestamp)
				{
					$scope.$apply(this.updatePotentialBusiness.bind(this));
					requestAnimationFrame(this.animationTick);
				}).bind(this);

			$timeout(this.tick, 1000);
			requestAnimationFrame(this.animationTick);

			this.load();
		}])
	.controller("BusinessPurchaseController", ["$modalInstance", "businessDefs",
		function($modalInstance, businessDefs)
		{
			this.businessDefs = businessDefs;
			this.selected = null;
			this.selectedName = "N/A";

			this.select =
				function(definition)
				{
					this.selected = definition;
					try
					{
						this.selectedName = definition.nameList[Math.floor(Math.random()*definition.nameList.length)] || definition.typeName;
					}
					catch(err)
					{
						this.selectedName = definition.typeName;
					}
				};

			this.accept =
				function()
				{
					$modalInstance.close(
						{
							selected:this.selected,
							name:this.selectedName
						});
				};

			this.cancel =
				function()
				{
					$modalInstance.dismiss('cancelled');
				};
		}])
	.directive("upgradeName",
		function()
		{
			return 	{
						restrict: 'AE',
						scope:	{
									upgrade: '='
								},
						template:'<span tooltip="{{upgrade.description}}">{{upgrade.name}}</span>'
					};
		})
	.directive("upgradeCost",
		function()
		{
			return 	{
						restrict:'AE',
						scope:	{
									upgrade: '='
								},
						template:'<span tooltip="Base cost for this upgrade is {{format(upgrade.baseCost)}}, cost scales by a factor of {{upgrade.costExponent}}.">{{format(upgrade.cost)}}</span>', //NB: don't use tue-comparison for the cost; would be reflected in its own purchase, which might confuse people - playtest to see if it does
						link:	function(scope, element, attributes)
								{
									scope.format = tue.format;
								}
					};
		})
	.directive("upgradeEffect",
		function()
		{
			return 	{
						restrict:'AE',
						scope:	{
									upgrade: '=',
									effect: '@'
								},
						template:'<span tooltip="You own {{upgrade.owned}} upgrades with a maximum of {{upgrade.limit}}.">{{effect}}</span>'
					};
		})
	.directive("upgradeButtonStd",
		function()
		{
			return 	{
						restrict:'AE',
						scope:	{
									activeTarget: '='
								},
						template:	'<button class="btn btn-default btn-spanning" ng-disabled="!activeTarget.affordable" ng-click="activeTarget.purchase()">Buy</button>'
					};
		})
	.directive("upgradeRowStd",
		function()
		{
			return 	{
						restrict: 'A', //you can't use it as an element because tables throw out non-tr elements before they get replaced by tr elements
						replace: true,
						scope:	{
									activeTarget: '=',
									effect: '@'
								},
						template:	'<tr>' +
										'<td upgrade-name upgrade="activeTarget"></td>' +
										'<td upgrade-cost upgrade="activeTarget"></td>' +
										'<td upgrade-effect upgrade="activeTarget" effect="{{effect}}"></td>' +
										'<td upgrade-button-std active-target="activeTarget"></td>' +
									'</tr>'
					};
		})
	.directive("upgradeButtonComparison",
		function()
		{
			return 	{
						restrict:'AE',
						scope:	{
									activeTarget: '=',
									potentialTarget: '='
								},
						template:	'<tue-action active-target="activeTarget" potential-target="potentialTarget" action="target.purchase()">' +
										'<button class="btn btn-default btn-spanning" ng-disabled="!activeTarget.affordable">Buy</button>' +
									'</tue-action>'
					};
		})
	.directive("upgradeRowComparison",
		function()
		{
			return 	{
						restrict: 'A', //you can't use it as an element because tables throw out non-tr elements before they get replaced by tr elements
						replace: true,
						scope:	{
									activeTarget: '=',
									potentialTarget: '=',
									effect: '@'
								},
						template:	'<tr>' +
										'<td upgrade-name upgrade="activeTarget"></td>' +
										'<td upgrade-cost upgrade="activeTarget"></td>' +
										'<td upgrade-effect upgrade="activeTarget" effect="{{effect}}"></td>' +
										'<td upgrade-button-comparison active-target="activeTarget" potential-target="potentialTarget"></td>' +
									'</tr>'
					};
		});
	/*.directive("upgradeTableProduct",
		function()
		{
			return 	{
						restrict: 'A', //you can't use it as an element because tables throw out non-tr elements before they get replaced by tr elements
						replace: true,
						scope:	{
									section: '@',
									business: '=',
									upgrade: '@',
									effect: '='
								},
						template:	'<table class="table table-bordered table-hover table-responsive no-select">' +
										'<thead>' +
											'<tr>' +
												'<th colspan="4" class="text-center"><span>{{section}}</span></th>' +
											'</tr>' +
											'<tr>' +
												'<td>Upgrade</td>' +
												'<td>Cost</td>' +
												'<td>Effect</td>' +
												'<td>Upgrade</td>' +
											'</tr>' +
										'</thead>' +
										'<tbody>' +
											'<tr upgrade-row-std upgrade="getUpgrade(product)" effect="{{effect}}" ng-repeat="product in business.products"></tr>' +
										'</tbody>' +
									'</table>',
						link: function(scope, iElement, iAttrs)
							{
								scope.getUpgrade =
									function(product)
									{
										return product.upgrades[scope.upgrade];
									};
							}
					};
		});*/