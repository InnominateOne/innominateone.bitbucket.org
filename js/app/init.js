tue = {};

tue.loadImportString =
	function()
	{
		return "";
	};

tue.toEngineeringNotation =
	function(input, places)
	{
		var negative = false;
		if(input < 0)
		{
			negative = true;
			input = -input;
		}
		places = places || 2;
		var value = Number(input);
		var threePower = 3*Math.floor(Math.log10(value)/3); //nearest lower power of 3
		var mantissa = value/Math.pow(10, threePower);
		var exponent = threePower;
		if(isNaN(mantissa))
		{
			mantissa = 0;
		}
		if(!isFinite(exponent))
		{
			exponent = 0;
		}
		if(negative)
		{
			mantissa = -mantissa;
		}
		return {
					mantissa: mantissa.toFixed(places),
					exponent: exponent
				};
	};

tue.arrayRemove = //is slow, don't use for large arrays
	function(obj, array)
	{
		var index = array.indexOf(obj);
		if(index >= 0)
		{
			array.splice(index, 1);
			return true;
		}
		return false;
	};

tue.format =
	function(input, places)
	{
		if(!isFinite(input))
		{
			return Infinity;
		}
		var formatted = tue.toEngineeringNotation(input, places);
		return formatted.mantissa + "e" + formatted.exponent;
	};

/*
Accumulates arbitrary numeric inputs and discharges the sum in quantised amounts.

Arguments:
* quantum - the quantised difference between acceptable output values
* initial - the initial value for the accumulator's buffer
* discharger - a function which, if specified, is called with the amount discharged
* limiter - a function which, if specified, is called on discharge to get an upper limit for the output
*/
tue.Accumulator =
	function(quantum, initial, discharger, limiter)
	{
		this.quantum = quantum || 1;
		this.accum = initial || 0;
		this.discharger = discharger || null;
		this.limiter = limiter || null;
	};

tue.Accumulator.prototype.accumulate =
	function(amount)
	{
		this.accum += amount;
	};

//Any excess in the buffer that isn't discharged will be modulo'd with the quantum (i.e. unused "chunks" are discarded).
//discharger, if it was specified, is called even if the output is 0.
tue.Accumulator.prototype.discharge =
	function()
	{
		var limit = this.accum;
		if(this.limiter)
		{
			limit = Math.min(this.accum, this.limiter());
		}
		var used = Math.max(Math.floor(limit/this.quantum)*this.quantum, 0);
		if(this.discharger)
		{
			this.discharger(used);
		}
		this.accum -= used;
		this.accum %= this.quantum; //discard any unused "whole" chunks
		return used;
	};

tue.Accumulator.prototype.synchronise =
	function(actual)
	{
		this.accum = actual.accum;
	};

tue.Accumulator.prototype.encode =
	function()
	{
		return this.accum;
	};

tue.Accumulator.decode =
	function(data, base)
	{
		base.accum = data;
	};

tue.defineUpgrade =
	function(obj, label, baseCost, costExponent, limit, name, description)
	{
		obj.upgrades = obj.upgrades || {};
		var data = obj.upgrades[label] =
			{
				owned: 0,
				baseCost: baseCost || 1,
				costExponent: costExponent || 1.5,
				limit: limit || Infinity,
				name: name || "",
				description: description || "",
				get cost()
					{
						if(this.owned >= this.limit)
						{
							return Infinity;
						}
						return this.baseCost*Math.pow(this.costExponent, this.owned);
					},
				get affordable()
					{
						return obj.cash >= this.cost;
					},
				purchase: function()
					{
						if(this.affordable) //this technically means that with infinite money we can still buy even capped upgrades, but who cares?
						{
							obj.cash -= this.cost;
							this.owned += 1;
							return true;
						}
						return false;
					},
				synchronise: function(actual)
					{
						this.owned = actual.owned;
					}
			};

		obj.encodeUpgrades =
			function()
			{
				var result = {};
				for(var label in obj.upgrades)
				{
					result[label] = obj.upgrades[label].owned;
				}
				return result;
			} || obj.encodeUpgrades;

		obj.decodeUpgrades =
			function(data)
			{
				for(var label in obj.upgrades)
				{
					obj.upgrades[label].owned = data[label];
				}
			} || obj.decodeUpgrades;
		return data;
	};

tue.events = {};
tue.events.potentialActionSet = "potentialActionSet";
tue.events.potentialActionClear = "potentialActionClear";
tue.events.appSave = "tueAppSave";
tue.events.appLoad = "tueAppLoad";
tue.events.dataUpdate = "tueDataUpdate";

tue.app = angular.module("tueApp", ["ui.bootstrap"]);

tue.app
	.config(
		function($logProvider)
			{
				$logProvider.debugEnabled(true);
			})
	.factory("FormatService",
		function()
		{
			return { format: tue.format };
		})
	.factory("DataService", ["$rootScope", "$log",
		function($rootScope, $log)
		{
			var service = {};
			var _namespaces = {};

			service.register =
				function(namespace)
				{
					if(namespace in _namespaces)
					{
						$log.error("DataService: Attempted to reregister namespace " + namespace + ".");
					}
					var space = _namespaces[namespace] = {};

					var view = service.view(namespace);
					view.set =
						function(key, value)
						{
							space[key] = value;
							$rootScope.$broadcast(tue.events.dataUpdate, namespace, key, value);
						};
					//no view.delete by design
					return view;
				};

			service.get =
				function(namespace, key)
				{
					if(namespace in _namespaces)
					{
						return _namespaces[namespace][key];
					}
					return undefined;
				};

			service.view =
				function(namespace)
				{
					var view = {};
					view.get = service.get.bind(undefined, namespace);
					return view;
				};

			return service;
		}])
	.filter('capitalise',
		function()
		{
			return	function(input, all)
					{
						return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){ return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); }) : '';
					}
		})
	.factory("AlertService", ["$rootScope", "$timeout",
		function($rootScope, $timeout)
		{
			var service = {};
			service.alerts = [];

			service.notify =
				function(message, type, timeout)
				{
					var alert =
						{
							message: message,
							type: type || "info" //valid types are "danger", "info", "success", "warning"
						};
					service.alerts.unshift(alert); //push if we wanted it to be a queue, but a stack is more user-friendly
					$rootScope.$broadcast("alertsUpdated");
					if(timeout)
					{
						$timeout(tue.arrayRemove.bind(undefined, alert, service.alerts), timeout);
					}
				};

			service.close =
				function(index)
				{
					service.alerts.splice(index || 0, 1);
					$rootScope.$broadcast("alertsUpdated");
				};

			service.closeAll =
				function()
				{
					service.alerts.splice(0, service.alerts.length);
					$rootScope.$broadcast("alertsUpdated"); //should put all of these in variables maybe, but who cares?
				};

			return service;
		}])
	.controller("TueAlertController", ["$scope", "AlertService",
		function($scope, AlertService)
		{
			this.alerts = AlertService.alerts;
			this.displayedAlerts = 3;

			this.closeAlert =
				function(index)
				{
					AlertService.close(index);
				};

			Object.defineProperty(this, "numAlerts",
				{
					get: function()
						{
							return AlertService.alerts.length;
						}
				});

			Object.defineProperty(this, "surplusAlerts",
				{
					get: function()
						{
							return Math.max(this.numAlerts - this.displayedAlerts, 0);
						}
				});
		}]);

tue.alert = tue.alert || {};
tue.alert.notify = //only used for notifying outside of an angular service
	function(message, type, timeout)
	{
		var alerter = angular.element('*[ng-app]').injector().get("AlertService");
		alerter.notify(message, type, timeout);
	};