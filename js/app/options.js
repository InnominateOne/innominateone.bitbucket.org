tue.app
	.controller("OptionsController", ["$rootScope", "$scope", "AlertService", "DataService",
		function($rootScope, $scope, AlertService, DataService)
		{
			var _view = DataService.register("options");

			this.update =
				function(key, value)
				{
					console.log("Updating entry " + key + " to " + value);
					_view.set(key, value);
				}

			window.onbeforeunload =
				function(event)
				{
					switch(_view.get("quitBehaviour"))
					{
						case "save":
							this.save();
							break;
						case "alert":
							event.preventDefault();
							return "Are you sure you want to quit?";
					}
				};

			this.save =
				function()
				{
				};

			this.load =
				function()
				{
				};

			this.deleteSave =
				function()
				{
				};

			this.categories = ["saving", "prestige"];
			this.activeCategory = this.categories[0];

			this.entries =
				{
					quitBehaviour: { value: "save" },
					autosave: { value: "true" },
					autosaveFrequency: { value: 30 }
				}

			/*this.categories =
				{
					saving:	{
								name: "Saving",
								entries:	[
												{
													name: "Save",
													type: "button",
													call: this.save //function to be called
												},
												{
													name: "Load",
													type: "button",
													call: this.load //function to be called
												},
												{
													name: "Delete",
													type: "button",
													call: this.deleteSave //function to be called
												},
												{
													id: "quitBehaviour",
													name: "Quit Behaviour",
													description: "What action the game should take when you attempt to quit: save, alert you (so you can manually save), or nothing.",
													type: "radio",
													options:	[
																	{
																		name: "Save",
																		value: "save"
																	},
																	{
																		name: "Alert",
																		value: "alert"
																	},
																	{
																		name: "Nothing",
																		value: "none"
																	}
																],
													value: "save"
												},
												{
													id: "autosave",
													name: "Autosave",
													description: "If enabled, autosaves the game at regular intervals.",
													type: "toggle",
													value: true
												},
												{
													id: "autosaveFrequency",
													name: "Autosave Frequency",
													description: "Time (in seconds) between autosaves.",
													type: "integer",
													value: 30
												}
											]
							}
				};
			this.activeCategory = this.categories.saving;*/

			/*$.each(this.categories,
				function(label, category)
				{
					$.each(category.entries,
						function(index, entry)
						{
							this.update(entry);
						}.bind(this));
				}.bind(this));*/
		}])
	.directive("radioButtons",
		function()
		{
			return	{
						restrict: "E",
						scope:	{
									id: "@",
									model: "=",
									options: "="
								},
						controller:	function($scope)
									{
										this.id = $scope.id;
										this.model = $scope.model;
									},
						link:	function(scope)
								{
									scope.alert = function() { console.log(scope.model); };
								},
						template:	'<div class="btn-group">' +
										'<label class="btn btn-default" ng-model="model.value" btn-radio="value" ng-change="options.update(id, model)" ng-click="alert()" ng-repeat="(value, label) in options">{{label}}</label>' +
									'</div>'
					};
		})
	.directive("radioOption",
		function()
		{
			return	{
						restrict: "E",
						transclude: true,
						replace: true,
						require: "^radioButtons",
						link:	function postLink(scope, element, attributes, radioButtonsController)
								{
									scope.id = radioButtonsController.id;
									scope.model = radioButtonsController.model;
									scope.alert = function() { console.log(scope.model); console.log(radioButtonsController.model); };
								},
						scope:	{
									value: "="
								},
						template: '<label class="btn btn-default" ng-model="model" ng-change="options.update(id, model)" btn-radio="value" ng-transclude ng-click="alert()"></label>'
					};
		});