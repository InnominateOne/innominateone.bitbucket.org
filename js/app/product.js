tue.product = tue.product || {};

tue.product.contractStockRateExponent = 1.5;
tue.product.contractCostExponent = 1.5;
tue.product.contractDiscountWeight = 20; //higher weight is lower discount per contract upgrade
tue.product.salesCostExponent = 1.5;
tue.product.salesDesireExponent = 1.2;
tue.product.salesMarkupScale = 0.2;
tue.product.fireSaleMultiplier = 0.25;

/*
perhaps add a flag to show whether the inflow/outflow was capped in the last tick?
i.e. if cash/cost capped, colour the buy rate box
if stock capped, colour the sell rate box
*/
tue.product.Product =
	function(business, definition)
	{
		this.business = business;
		this.def = definition;
		this.stock = 0; //this.def.initialStock || 0;

		this.stockAccum = new tue.Accumulator(1, 0,
			this.purchase.bind(this),
			function()
			{
				return this.cash/this.cost;
			}.bind(this));

		this.sellAccum = new tue.Accumulator(1, 0,
			this.sell.bind(this),
			function()
			{
				return this.stock;
			}.bind(this));

		tue.defineUpgrade(this, "contract", this.def.contractCost, tue.product.contractCostExponent, undefined, "Contract: " + this.def.name, "Supply contract upgrades increase the rate at which you automatically purchase products and lowers the cost per unit.");
		tue.defineUpgrade(this, "sales", this.def.salesCost, tue.product.salesCostExponent, undefined, "Sales: " + this.def.name, "Sales strategy upgrades increase the average number of units of a product each customer buys, and raises the value per unit.");

		Object.defineProperty(this, "cash",
			{
				get:function()
					{
						return this.business.cash;
					},
				set:function(value)
					{
						return this.business.cash = value;
					}
			});

		Object.defineProperty(this, "stockRate",
			{
				get:function()
					{
						var ups = this.upgrades.contract.owned;
						if(ups < 1)
						{
							return 0;
						}
						return this.def.stockRate*Math.pow(tue.product.contractStockRateExponent, ups - 1);
					}
			});
		Object.defineProperty(this, "cost",
			{
				get:function()
					{
						var scale = tue.product.contractDiscountWeight;
						return this.def.cost*scale/(scale + this.upgrades.contract.owned);
						//return tue.decayingFunction(this.def.cost, tue.product.contractDiscountWeight, this.upgrades.contract.owned);
					}
			});
		Object.defineProperty(this, "sellRate",
			{
				get:function()
					{
						var ups = this.upgrades.sales.owned + this.business.upgrades.advertising.owned;
						return this.def.sellRate*Math.pow(tue.product.salesDesireExponent, ups);
					}
			});
		Object.defineProperty(this, "totalSellRate",
			{
				get:function()
					{
						return this.sellRate*this.business.customerRate;
					}
			});
		Object.defineProperty(this, "actualSellRate",
			{
				get:function()
					{
						return Math.min(this.totalSellRate, this.stockRate);
					}
			});
		Object.defineProperty(this, "value",
			{
				get:function()
					{
						var ups = this.upgrades.sales.owned*(this.business.upgrades.advertising.owned + 1);
						return this.def.value*(1 + ups*tue.product.salesMarkupScale);
					}
			});
		Object.defineProperty(this, "costRate",
			{
				get:function()
					{
						return this.cost*this.stockRate;
					}
			});
		Object.defineProperty(this, "valueRate",
			{
				get:function()
					{
						return this.value*this.totalSellRate;
					}
			});
		Object.defineProperty(this, "fireSaleValue",
			{
				get:function()
					{
						return this.stock*this.value*tue.product.fireSaleMultiplier;
					}
			});
		Object.defineProperty(this, "cashRate",
			{
				get:function()
					{
						return this.actualSellRate*this.value - this.stockRate*this.cost;
					}
			});
		Object.defineProperty(this, "netStockRate",
			{
				get:function()
					{
						return this.stockRate - this.totalSellRate;
					}
			});
	};

tue.product.Product.prototype.update =
	function(customers)
	{
		this.stockAccum.accumulate(this.stockRate);
		this.stockAccum.discharge();

		this.sellAccum.accumulate(this.sellRate*customers);
		this.sellAccum.discharge();
	};

tue.product.Product.prototype.purchase =
	function(amount)
	{
		amount = (amount !== undefined)?amount:1;
		this.cash -= amount*this.cost;
		this.stock += amount;
	};

tue.product.Product.prototype.safePurchase =
	function(amount)
	{
		amount = (amount !== undefined)?amount:1;
		return this.purchase(Math.max(Math.min(amount, this.cash/this.cost)), 0);
	};

tue.product.Product.prototype.sell =
	function(amount)
	{
		amount = (amount !== undefined)?amount:1;
		amount = Math.max(Math.min(amount, this.stock), 0);
		this.cash += amount*this.value;
		this.stock -= amount;
	};

tue.product.Product.prototype.refund =
	function(amount)
	{
		amount = (amount !== undefined)?amount:1;
		amount = Math.max(Math.min(amount, this.stock), 0);
		this.cash += amount*this.cost; //cost, not value
		this.stock -= amount;
	};

tue.product.Product.prototype.synchronise =
	function(actual)
	{
		this.stock = actual.stock;
		this.stockAccum.synchronise(actual.stockAccum);
		this.sellAccum.synchronise(actual.sellAccum);

		for(var label in this.upgrades)
		{
			this.upgrades[label].synchronise(actual.upgrades[label]);
		}
	};

tue.product.Product.prototype.encode =
	function()
	{
		var result = {};
		result.def = this.business.products.indexOf(this);
		result.stock = this.stock;

		result.stockAccum = this.stockAccum.encode();
		result.sellAccum = this.sellAccum.encode();

		result.upgrades = this.encodeUpgrades();

		return result;
	};

tue.product.Product.decode =
	function(data)
	{
		var result = new tue.product.Product(data.business, data.business.def.productDefs[data.def]);
		result.stock = data.stock;
		tue.Accumulator.decode(data.stockAccum, result.stockAccum);
		tue.Accumulator.decode(data.sellAccum, result.sellAccum);

		result.decodeUpgrades(data.upgrades);

		return result;
	};