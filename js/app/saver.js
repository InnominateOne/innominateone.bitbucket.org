tue.app
	.controller("SaveController", ["$rootScope", "$scope", "$timeout", "AlertService",
		function($rootScope, $scope, $timeout)
		{
			this.save =
				function()
				{
					$rootScope.$broadcast(tue.events.appSave);
				};

			this.load =
				function()
				{
					$rootScope.$broadcast(tue.events.appLoad);
				};
		}]);