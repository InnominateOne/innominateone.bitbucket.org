tue.app
	.controller("UpgradeController", ["$rootScope", "$scope", "AlertService", "DataService",
		function($rootScope, $scope, AlertService, DataService)
		{
			this.format = tue.format;

			this.empire = DataService.get("empire", "empire");

			$rootScope.$on(tue.events.dataUpdate,
				function(event, namespace, key, value)
				{
					if(namespace === "empire" && key === "empire")
					{
						this.empire = value;
					}
				});

			this.categories = ["Empire"];
			this.upgrades = [];
			_activeCategory = null;
			tue.business.BusinessDefs.forEach(
				function(definition)
				{
					this.categories.push(definition.typeName);
				}, this);
			
			Object.defineProperty(this, "activeCategory",
				{
					get:function()
						{
							return _activeCategory;
						},
					set:function(value)
						{
							_activeCategory = value;
							this.upgrades = this.getUpgrades(_activeCategory);
						}
				});

			this.getUpgrades =
				function(category)
				{
					var result = [];
					var subject = this.empire;
					for(var label in subject.upgrades)
					{
						var index = label.search(category);
						if(index == 0)
						{
							result.push(subject.upgrades[label]);
						}
					}
					return result;
				};

				this.activeCategory = this.categories[0];
		}]);