app = angular.module("testApp", ["ui.bootstrap"]);

tue = {};
tue.events = {};
tue.events.potentialActionSet = "potentialActionSet";
tue.events.potentialActionClear = "potentialActionClear";

app
	.config(
		function($logProvider)
			{
				$logProvider.debugEnabled(true);
			})
	.controller("TestController", ["$scope", "$timeout",
		function($scope, $timeout)
		{
			this.active = [0, 1, 2, 3, 4];
			this.potential = $.extend(true, [], this.active);
			this.potentialAction = null;

			this.updatePotential =
				function()
				{
					this.potential = $.extend(true, this.potential, this.active);
					if(this.potentialAction !== null)
					{
						this.potentialAction();
					}
				};
			this.updatePotential();

			$scope.$on(tue.events.potentialActionSet,
				(function(event, action)
				{
					this.potentialAction = action;
					this.updatePotential();
				}).bind(this));

			$scope.$on(tue.events.potentialActionClear,
				(function(event, action)
				{
					if(this.potentialAction === action)
					{
						this.potentialAction = null;
						this.updatePotential();
					}
				}).bind(this));

			this.tick =
				(function()
				{
					$timeout(this.tick, 1000);
				}).bind(this);
			
			this.animationTick =
				(function(timestamp)
				{
					$scope.$apply(this.updatePotential.bind(this));
					requestAnimationFrame(this.animationTick);
				}).bind(this);

			$timeout(this.tick, 1000);
			requestAnimationFrame(this.animationTick);
		}])
	.directive("tueComparison",
		function()
		{
			var link =
				function($scope, element, attrs)
				{
					$scope.isPositive = angular.isDefined($scope.isPositive) ? $scope.isPositive : true;

					$scope.active =
						function()
						{
							return $scope.comparison({subject: $scope.activeSubject});
						};
					$scope.potential =
						function()
						{
							return $scope.comparison({subject: $scope.potentialSubject});
						};

					$scope.opinion =
						function()
						{
							var a = $scope.active();
							var p = $scope.potential();
							if(p == a)
							{
								return null;
							}
							else if(p > a)
							{
								return $scope.isPositive;
							}
							return !$scope.isPositive;
						};
				};

			return {
						restrict: 'E',
						scope:  {
									activeSubject: '=',
									potentialSubject: '=',
									comparison: '&', //used to determine if they are equal
									isPositive: '=?',
									transform: '&' //applied to transform the result into what we want
								},
						link:link,
						template:	'<span ng-class="{\'text-success\':opinion() === true, \'text-danger\':opinion() === false}">{{transform({result: potential()})}}</span>'
					};
		})
	.directive("tueAction",
		function()
		{
			var link =
				function($scope, element, attrs)
				{
					$scope.applyPotential =
						function()
						{
							$scope.action({target: $scope.potentialTarget});
						};
				};

			return {
						restrict: 'E',
						transclude: true,
						scope:  {
									activeTarget: '=',
									potentialTarget: '=',
									action: '&'
								},
						link: link,
						template: '<div ng-click="action({target: activeTarget})" ng-mouseenter="$emit(\'potentialActionSet\', applyPotential)" ng-mouseleave="$emit(\'potentialActionClear\', applyPotential)" ng-transclude></div>'
					};
		});